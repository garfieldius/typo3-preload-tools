
.PHONY: build
build: vendor/autoload.php

.PHONY: clean
clean:
	@rm -f vendor/autoload.php .php_cs.cache .phpunit.result.cache

.PHONY: clobber
clobber: clean
	@rm -rf vendor bin

.PHONY: test
test: bin/php-cs-fixer bin/phpunit
	@php bin/phpunit -c phpunit.xml
	@php bin/php-cs-fixer fix --config=.php_cs --verbose --dry-run

.PHONY: fix
fix: bin/php-cs-fixer
	@php bin/php-cs-fixer fix --config=.php_cs --verbose --diff

vendor/autoload.php bin/php-cs-fixer bin/phpunit: bin/composer.phar composer.json composer.lock
	@php bin/composer.phar install --no-suggest -n --ansi && touch vendor/autoload.php

bin/composer.phar:
	@php -r "@mkdir('bin');" \
	&& php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
	&& php -r "if (hash_file('sha384', 'composer-setup.php') !== 'e0012edf3e80b6978849f5eff0d4b4e4c79ff1609dd1e613307e16318854d24ae64f26d17af3ef0bf7cfb710ca74755a') { echo 'Installer corrupt' . PHP_EOL; unlink('composer-setup.php'); exit(1); }" \
	&& php composer-setup.php --install-dir=bin/ \
	&& php -r "unlink('composer-setup.php');"
