<?php
declare(strict_types=1);

/*
 * Copyright 2020 by Georg Großberger <contact@grossberger-ge.org>
 *
 * This is free software; it is provided under the terms of Apache License 2.0
 * See the file LICENSE or <https://www.apache.org/licenses/LICENSE-2.0> for details
 */

$EM_CONF[$_EXTKEY] = [
    'title'            => 'TYPO3 Preload Tools',
    'description'      => 'Helper to generate preload configurations for TYPO3',
    'version'          => '1.0.0',
    'state'            => 'stable',
    'category'         => 'misc',
    'clearCacheOnLoad' => 0,
    'constraints'      => [
        'depends'   => [],
        'conflicts' => [],
        'suggests'  => [],
    ],
];
