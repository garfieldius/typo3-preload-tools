<?php

/*
 * Copyright 2020 by Georg Großberger <contact@grossberger-ge.org>
 *
 * This is free software; it is provided under the terms of Apache License 2.0
 * See the file LICENSE or <https://www.apache.org/licenses/LICENSE-2.0> for details
 */

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['pageLoadedFromCache'][1580828911] =
    \GrossbergerGeorg\PreloadTools\StatusTracker::class . '->loadedFromCache';

if (empty($GLOBALS['TYPO3_CONF_VARS']['LOG']['GrossbergerGeorg'])) {
    $GLOBALS['TYPO3_CONF_VARS']['LOG']['GrossbergerGeorg'] = [];
}

$GLOBALS['TYPO3_CONF_VARS']['LOG']['GrossbergerGeorg']['PreloadTools'] = [
    'writerConfiguration' => [
        \TYPO3\CMS\Core\Log\LogLevel::DEBUG => [
            \GrossbergerGeorg\PreloadTools\ConsoleWriter::class => [],
        ]
    ]
];
