<?php
declare(strict_types=1);
namespace GrossbergerGeorg\PreloadTools\Tests\Fixtures;

/*
 * Copyright 2020 by Georg Großberger <contact@grossberger-ge.org>
 *
 * This is free software; it is provided under the terms of Apache License 2.0
 * See the file LICENSE or <https://www.apache.org/licenses/LICENSE-2.0> for details
 */

/**
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class MethodReturnType
{
}
