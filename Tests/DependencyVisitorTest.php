<?php
declare(strict_types=1);
namespace GrossbergerGeorg\PreloadTools\Tests;

/*
 * Copyright 2020 by Georg Großberger <contact@grossberger-ge.org>
 *
 * This is free software; it is provided under the terms of Apache License 2.0
 * See the file LICENSE or <https://www.apache.org/licenses/LICENSE-2.0> for details
 */

use GrossbergerGeorg\PreloadTools\DependencyVisitor;
use GrossbergerGeorg\PreloadTools\Tests\Fixtures\FunctionParameterType;
use GrossbergerGeorg\PreloadTools\Tests\Fixtures\FunctionReturnType;
use GrossbergerGeorg\PreloadTools\Tests\Fixtures\ImplTrait;
use GrossbergerGeorg\PreloadTools\Tests\Fixtures\MainClass;
use GrossbergerGeorg\PreloadTools\Tests\Fixtures\MethodParameterType;
use GrossbergerGeorg\PreloadTools\Tests\Fixtures\MethodReturnType;
use GrossbergerGeorg\PreloadTools\Tests\Fixtures\NullInterface;
use GrossbergerGeorg\PreloadTools\Tests\Fixtures\ParentClass;
use GrossbergerGeorg\PreloadTools\Tests\Fixtures\PropertyType;
use PhpParser\NodeTraverser;
use PhpParser\NodeVisitor\NameResolver;
use PhpParser\ParserFactory;
use PHPUnit\Framework\TestCase;
use Psr\Log\NullLogger;

/**
 * @covers \GrossbergerGeorg\PreloadTools\DependencyVisitor
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class DependencyVisitorTest extends TestCase
{
    public function testResolveDependencies(): void
    {
        $nameResolver = new NameResolver();

        $subject = new DependencyVisitor();
        $subject->setLogger(new NullLogger());
        $subject->setNameContext($nameResolver->getNameContext());

        $traverser = new NodeTraverser();
        $traverser->addVisitor($nameResolver);
        $traverser->addVisitor($subject);

        $code = file_get_contents((new \ReflectionClass(MainClass::class))->getFileName());
        $parser = (new ParserFactory())->create(ParserFactory::PREFER_PHP7);
        $ast = $parser->parse($code);

        $expected = [
            ParentClass::class,
            NullInterface::class,
            ImplTrait::class,
            PropertyType::class,
            MethodReturnType::class,
            MethodParameterType::class,
            FunctionReturnType::class,
            FunctionParameterType::class,
        ];

        $traverser->traverse($ast);

        $actual = $subject->getDependencies();

        $shorten = static function ($classes) {
            return array_map(function ($class) {
                return substr($class, 30);
            }, $classes);
        };

        static::assertEquals($shorten($expected), $shorten($actual));
    }
}
