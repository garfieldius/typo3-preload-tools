<?php
declare(strict_types=1);
namespace GrossbergerGeorg\PreloadTools\Tests;

/*
 * Copyright 2020 by Georg Großberger <contact@grossberger-ge.org>
 *
 * This is free software; it is provided under the terms of Apache License 2.0
 * See the file LICENSE or <https://www.apache.org/licenses/LICENSE-2.0> for details
 */

use GrossbergerGeorg\PreloadTools\DependencyVisitor;
use GrossbergerGeorg\PreloadTools\PreloadCompiler;
use GrossbergerGeorg\PreloadTools\Tests\Fixtures\MainClass;
use GrossbergerGeorg\PreloadTools\Tests\Fixtures\NullInterface;
use GrossbergerGeorg\PreloadTools\Tests\Fixtures\ParentClass;
use GrossbergerGeorg\PreloadTools\Tests\Fixtures\PropertyType;
use org\bovigo\vfs\vfsStream;
use PhpParser\Parser\Multiple;
use PhpParser\ParserFactory;
use PHPUnit\Framework\TestCase;
use Psr\Log\NullLogger;
use TYPO3\CMS\Core\Core\ApplicationContext;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class PreloadCompilerTest extends TestCase
{
    /**
     * @var \org\bovigo\vfs\vfsStreamDirectory
     */
    private $fs;

    protected function setUp(): void
    {
        $this->fs = vfsStream::setup('app', null, [
            'var'    => [],
            'public' => [
                'index.php' => '<?php echo "TYPO3 frontend";',
                'typo3conf' => [
                    'AdditionalConfiguration.php' => '<?php $config = [];',
                ],
            ],
            'config' => [
                'Preload.php' => '<?php echo "Preload script comes here";'
            ],
        ]);

        Environment::initialize(
            new ApplicationContext('Testing'),
            true,
            true,
            $this->fs->url(),
            $this->fs->getChild('public')->url(),
            $this->fs->getChild('var')->url(),
            $this->fs->getChild('config')->url(),
            $this->fs->getChild('public/index.php')->url(),
            'UNIX'
        );
    }

    public function testAddFiles()
    {
        $subject = $this->createSubject();
        $subject->addFiles([
            $this->fs->getChild('public/index.php')->url(),
            $this->fs->getChild('public/typo3conf/AdditionalConfiguration.php')->url(),
        ]);

        $code = $this->getCompiledCode($subject);
        static::assertStringContainsString('public/index.php', $code);
    }

    public function testAddFilesSkipsAlreadyAdded()
    {
        $subject = $this->createSubject();
        $subject->addFiles([
            $this->fs->getChild('public/index.php')->url(),
            $this->fs->getChild('public/typo3conf/AdditionalConfiguration.php')->url(),
            $this->fs->getChild('public/index.php')->url(),
        ]);

        $code = $this->getCompiledCode($subject);
        static::assertSame(1, substr_count($code, 'public/index.php'));
    }

    public function testAddFilesThrowsExceptionOnNotExistingFile()
    {
        $this->expectException(\ErrorException::class);
        $subject = $this->createSubject();
        $subject->addFiles([
            $this->fs->getChild('public/index.php')->url(),
            $this->fs->getChild('public')->url() . '/i/do/not/exist',
        ]);
    }

    public function testAddClasses()
    {
        $ast = [];
        $parser = $this->createPartialMock(Multiple::class, ['parse']);
        $parser->expects(static::any())->method('parse')->willReturn($ast);

        $factory = $this->createMock(ParserFactory::class);
        $factory->expects(static::any())->method('create')->willReturn($parser);
        GeneralUtility::addInstance(ParserFactory::class, $factory);

        $mainDependencyRecorder = $this->createPartialMock(DependencyVisitor::class, ['getDependencies']);
        $mainDependencyRecorder->setLogger(new NullLogger());
        $mainDependencyRecorder->expects(static::any())->method('getDependencies')->willReturn([
            ParentClass::class,
            NullInterface::class,
            PropertyType::class,
        ]);

        GeneralUtility::addInstance(DependencyVisitor::class, $mainDependencyRecorder);

        $parentDependencyResolver = $this->createPartialMock(DependencyVisitor::class, ['getDependencies']);
        $parentDependencyResolver->setLogger(new NullLogger());
        $parentDependencyResolver->expects(static::any())->method('getDependencies')->willReturn([]);

        // Parent class
        GeneralUtility::addInstance(DependencyVisitor::class, $parentDependencyResolver);
        // Null interface
        GeneralUtility::addInstance(DependencyVisitor::class, $parentDependencyResolver);
        // Impl trait
        GeneralUtility::addInstance(DependencyVisitor::class, $parentDependencyResolver);

        $subject = $this->createSubject();
        $subject->addClasses([MainClass::class]);
        $code = $this->getCompiledCode($subject);

        static::assertStringContainsString(var_export(MainClass::class, true), $code);
        static::assertStringContainsString(var_export(ParentClass::class, true), $code);
        static::assertStringContainsString(var_export(NullInterface::class, true), $code);
        static::assertStringContainsString(var_export(PropertyType::class, true), $code);
    }

    private function createSubject(): PreloadCompiler
    {
        $subject = new PreloadCompiler();
        $subject->setLogger(new NullLogger());

        return $subject;
    }

    /**
     * @param PreloadCompiler $subject
     * @return false|string
     */
    private function getCompiledCode(PreloadCompiler $subject)
    {
        $target = $this->fs->getChild('config/Preload.php')->url();
        $subject->writeTo($target);
        $code = file_get_contents($target);

        return $code;
    }
}
