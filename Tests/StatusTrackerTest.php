<?php
declare(strict_types=1);
namespace GrossbergerGeorg\PreloadTools\Tests;

/*
 * Copyright 2020 by Georg Großberger <contact@grossberger-ge.org>
 *
 * This is free software; it is provided under the terms of Apache License 2.0
 * See the file LICENSE or <https://www.apache.org/licenses/LICENSE-2.0> for details
 */

use GrossbergerGeorg\PreloadTools\StatusTracker;
use PHPUnit\Framework\TestCase;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

/**
 * @covers \GrossbergerGeorg\PreloadTools\StatusTracker
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class StatusTrackerTest extends TestCase
{
    /**
     * @dataProvider modeNameData
     * @param bool $callCached
     * @param bool $hasUserInt
     * @param string $expected
     */
    public function testModeName(bool $callCached, bool $hasUserInt, string $expected): void
    {
        $GLOBALS['TSFE'] = $this->createMock(TypoScriptFrontendController::class);
        $GLOBALS['TSFE']->expects(static::any())->method('isINTincScript')->willReturn($hasUserInt);

        $subject = new StatusTracker();

        if ($callCached) {
            $subject->loadedFromCache();
        }

        $actual = $subject->getRuntimeModeName();

        static::assertEquals($expected, $actual);
    }

    public function modeNameData(): array
    {
        return [
            'No hook, no user int'     => [false, false, 'new'],
            'No hook, with user int'   => [false, true, 'new'],
            'With hook, no user int'   => [true, false, 'cached_full'],
            'With hook, with user int' => [true, true, 'cached_int'],
        ];
    }
}
