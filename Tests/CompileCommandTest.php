<?php
declare(strict_types=1);
namespace GrossbergerGeorg\PreloadTools\Tests;

/*
 * Copyright 2020 by Georg Großberger <contact@grossberger-ge.org>
 *
 * This is free software; it is provided under the terms of Apache License 2.0
 * See the file LICENSE or <https://www.apache.org/licenses/LICENSE-2.0> for details
 */

use GrossbergerGeorg\PreloadTools\CompileCommand;
use GrossbergerGeorg\PreloadTools\PreloadCompiler;
use GrossbergerGeorg\PreloadTools\Tests\Fixtures\MainClass;
use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use TYPO3\CMS\Core\Core\ApplicationContext;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @covers \GrossbergerGeorg\PreloadTools\CompileCommand
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class CompileCommandTest extends TestCase
{
    public function testExecute(): void
    {
        $fs = vfsStream::setup('app', null, [
            'public' => [
                'index.php' => '<?php echo "hello";'
            ],
            'config' => [],
            'var'    => [
                'data.php'    => file_get_contents(realpath(__DIR__) . '/Fixtures/input_list.php'),
                'preload.php' => '',
            ],
        ]);
        Environment::initialize(
            new ApplicationContext('Testing'),
            true,
            true,
            $fs->url(),
            $fs->getChild('public')->url(),
            $fs->getChild('var')->url(),
            $fs->getChild('config')->url(),
            $fs->getChild('public/index.php')->url(),
            'UNIX'
        );

        $input = new ArrayInput([
            '--add-file' => [$fs->getChild('public/index.php')->url()],
            '--input'    => $fs->getChild('var/data.php')->url(),
            '--output'   => '-',
        ]);

        $compiler = $this->createMock(PreloadCompiler::class);
        $compiler->expects(static::once())->method('addClasses')->with(static::equalTo([MainClass::class]));
        $compiler->expects(static::once())->method('addFiles')->with(static::equalTo([
            $fs->getChild('public/index.php')->url(),
        ]));
        $compiler->expects(static::once())->method('writeTo')->with('php://stdout');

        GeneralUtility::addInstance(PreloadCompiler::class, $compiler);

        $subject = new CompileCommand();
        $subject->run($input, new NullOutput());
    }

    public function testInputMustBeAFile()
    {
        $this->expectException(\ErrorException::class);

        $input = new ArrayInput([
            '--input' => '/a65f/c6c6/bf96.php',
        ]);
        $subject = new CompileCommand();
        $subject->run($input, new NullOutput());
    }

    public function testInputMustReturnAnArray()
    {
        $this->expectException(\ErrorException::class);
        $fs = vfsStream::setup('app', null, ['preload.php' => '<?php return null;']);

        $input = new ArrayInput([
            '--input' => $fs->getChild('preload.php')->url(),
        ]);
        $subject = new CompileCommand();
        $subject->run($input, new NullOutput());
        $this->expectException(\ErrorException::class);
    }
}
