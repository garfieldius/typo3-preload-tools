# TYPO3 Preload Tools

This TYPO3 extensions provides a CLI command to generate a PHP 7.4+
preload PHP file from the data of package `grossberger-georg/typo3-track-loaded`.

The list of loaded classes of that package is required, otherwise this
command has no data to work with.

## Installation

In a TYPO3 project that is based on composer, run `composer req --dev grossberger-georg/typo3-preload-tools`
and install the extensions. Use the extension manager or the
[TYPO3 console](https://github.com/TYPO3-Console/TYPO3-Console) command `typo3cms install:generatepackagestates`

## Usage

The package `grossberger-georg/typo3-track-loaded` is installed as a
dependency. With it, open the TYPO3 frontend and visit a couple pages.
This will create a file `loaded_new.php` inside the `var` folder of the
installation.

If the caches are active and pages are visited again, files name `loaded_cached_full.php`
and `loaded_cached_int.php` will be created as well. This files store
the loaded classes of a request, depending on the cache state:

* `_cached_full` contains all classes used when serving a fully cached
  page.
* `_cached_int` contains all classes used for cached pages that contain
   USER_INT and COA_INT cObjects.
* `_new` contains all classes for serving a page that is not in the
  cache and needs to be generated.

With this list(s), the command `typo3cms preload:compile` will generate
a PHP file that can be used for preloading. With the default settings
it will take the list of `_new` and write a script into the file `var/preload.php`.
This script can then be set as the PHP setting `opcache.preload`.

## Options

The command can be customized with the following options:

-i|--input FILE

By default, the file `var/loaded_new.php` is read. To use a different list,
use this switch with the file path, absolute or relative to the current
working directory: `typo3 preload:compile -i var/loaded_cached_full.php`

-o|--output FILE|-

The script is written to the file `var/preload.php`. To change this, add
this switch with the file path, absolute or relative to the current working
directory. All folders in the path must exists, they are not created:
`mkdir -p var/php; typo3 preload:compile -o var/php/preload.php`

Use the value `-` to write to stdout: `typo3 preload:compile -o - > config/preload.php`

-f|--add-file

By default it will not only load classes, but also the following files:

* `public/index.php`
* `public/typo3conf/AdditonalConfiguration.php`
* `public/typo3conf/PackageStates.php`

This list can be customized by using the -f options. It can be used several
times to add more files or use different paths to the files

`typo3 preload:compile -f web/index.php -f private/typo3conf/AdditionalConfiguration.php`

> Important:
> * Those files must be actual source files, not (symlinks).
> * If one `-f` option is used, the defaults are discared!

## Considerations

Which list serves best, depends on the size and complexity of the
installation. This needs to be tested, start with `new` and then
check if the pages are faster or slower, on average, when using the
`cached_int` and then `cached_full`. There is no fixed best-practice,
it really depends.

Also the list of additional files to add to the preload may vary. The
defaults are present on, almost, every system and are loaded on every
frontend request. It is common to have several `AdditionalConfiguration`
files, which prepare the setup to the current environment. To add those
can be a helpful as well.

When generating the lists (see the section *Usage* above), make sure to
run TYPO3 in a setup as close as possible to the live environment. This
ensures that classes, usually not used by *Development*, but *Production*
are not missing, or *Development* classes are preloaded in *Production*.
eg: commonly, the caches use the `NullBackend` on dev, but `RedisBackend` or
something similar on live, hence it would make sense to have the latter in
the list not the former.

## License

[Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0)
