<?php
declare(strict_types=1);
namespace GrossbergerGeorg\PreloadTools;

/*
 * Copyright 2020 by Georg Großberger <contact@grossberger-ge.org>
 *
 * This is free software; it is provided under the terms of Apache License 2.0
 * See the file LICENSE or <https://www.apache.org/licenses/LICENSE-2.0> for details
 */

use ErrorException;
use PhpParser\NodeTraverser;
use PhpParser\NodeVisitor\NameResolver;
use PhpParser\ParserFactory;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use ReflectionClass;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\StringUtility;

/**
 * Compile a list of classes and files for preloading
 *
 * @author Georg Großberger <g.grossberger@supseven.at>
 */
class PreloadCompiler implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    private $classes = [];

    private $files = [];

    /**
     * Add the classes in the given array
     *
     * Every item must be a fully qualified class name
     *
     * @param array $classes
     */
    public function addClasses(array $classes): void
    {
        $loaded = [];

        foreach ($classes as $class) {
            $this->addClass($class, $loaded);
        }
    }

    /**
     * Add the given files
     *
     * Every item must be a file path within the project path
     *
     * @param array $files
     */
    public function addFiles(array $files): void
    {
        $projectPath = Environment::getProjectPath();
        $strip = strlen($projectPath);
        $loaded = [];

        foreach ($files as $file) {
            if (!is_file($file)) {
                throw new ErrorException('File ' . $file . ' not found');
            }

            if (StringUtility::beginsWith($file, $projectPath)) {
                $file = substr($file, $strip);
            }

            $target = '/' . ltrim($file, '/');

            if (isset($loaded[$target])) {
                $this->logger->notice(sprintf('File %s already added', $file));
            } else {
                $this->logger->info(sprintf('Adding %s as %s', $file, $target));
                $this->files[] = $target;
                $loaded[$target] = true;
            }
        }
    }

    /**
     * Write the code to the given file
     *
     * @param string $target
     */
    public function writeTo(string $target): void
    {
        $content = $this->compileCode();
        file_put_contents($target, $content);
    }

    /**
     * Generate a string of PHP code from classes and files
     *
     * @return string
     */
    private function compileCode(): string
    {
        $template = file_get_contents(realpath(dirname(__DIR__)) . '/Resources/Private/Templates/Preload.php.tmpl');
        $arrayToListString = function (array $list): string {
            return implode(",\n    ", array_map(function ($class) {
                return var_export($class, true);
            }, $list));
        };

        $markers = [
            '{{ classes }}' => $arrayToListString($this->classes),
            '{{ files }}'   => $arrayToListString($this->files),
            '{{ date }}'    => date('Y.m.d H:i:s'),
        ];

        $this->logger->info(sprintf(
            'Preload contains %d classes and %d files',
            count($this->classes),
            count($this->files)
        ));

        return str_replace(array_keys($markers), array_values($markers), $template);
    }

    /**
     * Add the given classes to the list and check its source for dependencies
     *
     * @param string $class
     * @param array $visited
     * @param string $dependent
     */
    private function addClass(string $class, array &$visited, string $dependent = ''): void
    {
        if (isset($visited[$class])) {
            return;
        }

        $visited[$class] = true;

        if (!class_exists($class) && !interface_exists($class) && !trait_exists($class)) {
            if (!$dependent) {
                $this->logger->notice('Cannot load class ' . $class . ', excluding it from preload');
            } else {
                $this->logger->error('Cannot load ' . $class . ', required by ' . $dependent . ', excluding it');
            }

            return;
        }

        $sourceFile = (new ReflectionClass($class))->getFileName();

        if (!$sourceFile) {
            $this->logger->notice('Class ' . $class . ' appears to be a built-in, excluding it from preload');

            return;
        }

        $this->logger->debug(sprintf('Class %s has source file %s', $class, $sourceFile));

        if ($dependent) {
            $dependent .= ' > ';
        }

        $dependent .= $class;

        $this->logger->info('Adding dependencies of ' . $dependent);

        foreach ($this->loadDependenciesFromFile($sourceFile) as $dependency) {
            $this->addClass($dependency, $visited, $dependent);
        }

        $this->classes[] = $class;
    }

    private function loadDependenciesFromFile(string $sourceFile): array
    {
        $sourceCode = file_get_contents($sourceFile);
        $parser = GeneralUtility::makeInstance(ParserFactory::class)->create(ParserFactory::PREFER_PHP7);
        $ast = $parser->parse($sourceCode);
        $traverser = GeneralUtility::makeInstance(NodeTraverser::class);
        $nameResolver = GeneralUtility::makeInstance(NameResolver::class);
        $dependencyRecorder = GeneralUtility::makeInstance(DependencyVisitor::class);
        $dependencyRecorder->setNameContext($nameResolver->getNameContext());
        $traverser->addVisitor($nameResolver);
        $traverser->addVisitor($dependencyRecorder);
        $traverser->traverse($ast);

        return $dependencyRecorder->getDependencies();
    }
}
