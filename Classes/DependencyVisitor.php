<?php
declare(strict_types=1);
namespace GrossbergerGeorg\PreloadTools;

/*
 * Copyright 2020 by Georg Großberger <contact@grossberger-ge.org>
 *
 * This is free software; it is provided under the terms of Apache License 2.0
 * See the file LICENSE or <https://www.apache.org/licenses/LICENSE-2.0> for details
 */

use PhpParser\NameContext;
use PhpParser\Node;
use PhpParser\Node\Name;
use PhpParser\Node\Name\FullyQualified;
use PhpParser\Node\NullableType;
use PhpParser\Node\Stmt\Class_;
use PhpParser\Node\Stmt\ClassMethod;
use PhpParser\Node\Stmt\Function_;
use PhpParser\Node\Stmt\Property;
use PhpParser\NodeVisitorAbstract;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

/**
 * NodeVisitor that records class dependencies
 *
 * - parent class
 * - implemented interfaces
 * - used traits
 * - type hints in properties, arguments and returns
 *
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class DependencyVisitor extends NodeVisitorAbstract implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var NameContext
     */
    private $nameContext;

    private $dependencies = [];

    /**
     * @param NameContext $nameContext
     */
    public function setNameContext(NameContext $nameContext): void
    {
        $this->nameContext = $nameContext;
    }

    public function enterNode(Node $node)
    {
        switch (true) {
            case $node instanceof Class_:
                $this->addClassDependencies($node);
                break;

            case $node instanceof Property:
                $this->addPropertyTypes($node);
                break;

            case $node instanceof Function_:
            case $node instanceof ClassMethod:
                $this->addFunctionTypes($node);
                break;
        }
    }

    public function getDependencies(): array
    {
        $this->logger->debug('Got dependencies', $this->dependencies);

        return $this->dependencies;
    }

    /**
     * @param Class_ $node
     */
    private function addClassDependencies(Class_ $node): void
    {
        $this->logger->debug('Checking dependencies of class node ' . $node->name);

        if ($node->extends) {
            $this->dependencies[] = $node->extends->toString();
        }

        foreach ($node->implements as $implement) {
            $this->dependencies[] = $implement->toString();
        }

        foreach ($node->getTraitUses() as $traitUse) {
            foreach ($traitUse->traits as $trait) {
                if (!$trait instanceof FullyQualified) {
                    $trait = $this->nameContext->getResolvedClassName($trait);
                }

                $this->dependencies[] = $trait->toString();
            }
        }
    }

    private function addPropertyTypes(Property $node) :void
    {
        if ($node->type) {
            $type = $node->type;

            if ($type instanceof NullableType) {
                $type = $type->type;
            }

            if ($type instanceof FullyQualified) {
                $this->dependencies[] = $type->toString();
            }
        }
    }

    /**
     * @param Function_|ClassMethod $node
     */
    private function addFunctionTypes(Node $node): void
    {
        $types = [
            $node->getReturnType(),
        ];

        foreach ($node->params as $param) {
            $types[] = $param->type;
        }

        foreach ($types as $type) {
            if ($type instanceof NullableType) {
                $type = $type->type;
            }

            if ($type instanceof Name && !$type instanceof FullyQualified) {
                $type = $this->nameContext->getResolvedClassName($type);
            }

            if ($type instanceof FullyQualified) {
                $this->logger->debug('Adding class ' . $type->toString() . ' from type');
                $this->dependencies[] = $type->toString();
            }
        }
    }
}
