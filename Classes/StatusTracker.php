<?php
declare(strict_types=1);
namespace GrossbergerGeorg\PreloadTools;

/*
 * Copyright 2020 by Georg Großberger <contact@grossberger-ge.org>
 *
 * This is free software; it is provided under the terms of Apache License 2.0
 * See the file LICENSE or <https://www.apache.org/licenses/LICENSE-2.0> for details
 */

/**
 * Helper to determine if the page was loaded from cache
 *
 * @author Georg Großberger <g.grossberger@supseven.at>
 */
class StatusTracker
{
    /**
     * @var bool
     */
    private static $fromCache = false;

    /**
     * Set the internal "loaded-from-cache" flag to true
     *
     * Called via a hook/event
     */
    public function loadedFromCache(): void
    {
        static::$fromCache = true;
    }

    /**
     * Returns the key used by the dumper to determine "mode"
     *
     * Is one of
     *
     * - "new": The page was newly generated
     * - "cached_int": The page was loaded from cache, but contains uncached cObjects
     * - "cached_full": The page was loaded from cache, no additional processing was done
     *
     * @return string
     */
    public function getRuntimeModeName(): string
    {
        $key = 'new';

        if (static::$fromCache && !empty($GLOBALS['TSFE'])) {
            $key = $GLOBALS['TSFE']->isINTincScript() ? 'cached_int' : 'cached_full';
        }

        return $key;
    }
}
