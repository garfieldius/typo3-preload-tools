<?php
declare(strict_types=1);
namespace GrossbergerGeorg\PreloadTools;

/*
 * Copyright 2020 by Georg Großberger <contact@grossberger-ge.org>
 *
 * This is free software; it is provided under the terms of Apache License 2.0
 * See the file LICENSE or <https://www.apache.org/licenses/LICENSE-2.0> for details
 */

use ErrorException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Compile a list of preload classes and files
 *
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class CompileCommand extends Command
{
    protected function configure()
    {
        $this->setHelp(
            <<<EOF
Compile a list of classes and files and write it into a PHP file which
can be used as opcache.preload script

This requires the data of package grossberger-georg/typo3-track-loaded
to be in the file specified by option --input|-i

The -f switch can be used to add custom AdditionalConfiguration.php
files or similar. By default, the compiler will add:

* {web-dir}/index.php
* {web-dir}/typo3conf/PackageStates.php
* {web-dir}/typo3conf/AdditionalConfiguration.php

If only one additional file is given, the above list is discarded and
they have to be added as well as switches, if they should be included.
EOF
        );

        $this->setDescription('Compile a preload.php list from data of grossberger-georg/typo3-track-loaded');

        $this->addOption(
            'input',
            'i',
            InputOption::VALUE_REQUIRED,
            'File path pattern containing the dumps of loaded classes',
            Environment::getVarPath() . '/loaded_new.php'
        );

        $this->addOption(
            'output',
            'o',
            InputOption::VALUE_REQUIRED,
            'File path to write the code into. Use - to write to stdout',
            Environment::getProjectPath() . '/config/preload.php'
        );

        $this->addOption(
            'add-file',
            'f',
            InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY,
            'Additional file to add to the preload list. Will be added as relative to TYPO3_PATH_APP',
            ['public/index.php', 'public/typo3conf/AdditionalConfiguration.php', 'public/typo3conf/PackageStates.php']
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ConsoleWriter::setOutput($output);

        $file = $input->getOption('input');

        if (!is_file($file)) {
            throw new ErrorException('Input file not found in ' . $file);
        }

        $classes = require $file;

        if (!is_array($classes)) {
            throw new ErrorException('Input file does not return a PHP array');
        }

        $compiler = GeneralUtility::makeInstance(PreloadCompiler::class);
        $compiler->addClasses(array_keys($classes));

        $files = $input->getOption('add-file');

        if (is_array($files)) {
            $compiler->addFiles($files);
        }

        $target = $input->getOption('output');

        if ($target === '-') {
            $target = 'php://stdout';
        }

        $compiler->writeTo($target);

        return 0;
    }
}
