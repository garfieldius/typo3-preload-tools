<?php
declare(strict_types=1);
namespace GrossbergerGeorg\PreloadTools;

/*
 * Copyright 2020 by Georg Großberger <contact@grossberger-ge.org>
 *
 * This is free software; it is provided under the terms of Apache License 2.0
 * See the file LICENSE or <https://www.apache.org/licenses/LICENSE-2.0> for details
 */

use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\Log\LogLevel;
use TYPO3\CMS\Core\Log\LogRecord;
use TYPO3\CMS\Core\Log\Writer\AbstractWriter;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * Log writer that writes to a symfony/console output object
 *
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class ConsoleWriter extends AbstractWriter
{
    /**
     * Stolen from \Helhum\Typo3Console\Log\Writer\ConsoleWriter
     *
     * @var array
     */
    protected $severityTagMapping = [
        LogLevel::EMERGENCY => '<error>|</error>',
        LogLevel::ALERT     => '<error>|</error>',
        LogLevel::CRITICAL  => '<error>|</error>',
        LogLevel::ERROR     => '<fg=red>|</fg=red>',
        LogLevel::WARNING   => '<fg=yellow;options=bold>|</fg=yellow;options=bold>',
        LogLevel::NOTICE    => '<fg=yellow>|</fg=yellow>',
        LogLevel::INFO      => '<info>|</info>',
        LogLevel::DEBUG     => '|',
    ];

    /**
     * @var OutputInterface
     */
    private static $msg;

    /**
     * @var OutputInterface
     */
    private static $err;

    public static function setOutput(OutputInterface $output): void
    {
        self::$msg = $output;

        if ($output instanceof ConsoleOutput && $output->getErrorOutput()) {
            self::$err = $output->getErrorOutput();
        } else {
            self::$err = $output;
        }
    }

    public function writeLog(LogRecord $record)
    {
        $output = $record->getLevel() < LogLevel::WARNING ? self::$err : self::$msg;
        $message = LogLevel::getName($record->getLevel()) . ' - ' . $record->getMessage();

        $outputMode = OutputInterface::OUTPUT_NORMAL;

        if ($record->getData()) {
            $data = $record->getData();

            if (is_array($data) && array_key_exists('__', $data) && count($data) === 1) {
                $data = $data['__'];
            }

            $message = DebuggerUtility::var_dump(
                $data,
                $message,
                8,
                true,
                $output->isDecorated(),
                true
            );

            if ($output->isDecorated()) {
                $outputMode = OutputInterface::OUTPUT_RAW;
            }
        } elseif ($output->isDecorated()) {
            $message = str_replace('|', $message, $this->severityTagMapping[$record->getLevel()]);
        }

        switch ($record->getLevel()) {
            case LogLevel::DEBUG:
                $severity = OutputInterface::VERBOSITY_DEBUG;
                break;
            case LogLevel::INFO:
                $severity = OutputInterface::VERBOSITY_VERY_VERBOSE;
                break;
            case LogLevel::NOTICE:
                $severity = OutputInterface::VERBOSITY_VERBOSE;
                break;
            case LogLevel::WARNING:
                $severity = OutputInterface::VERBOSITY_NORMAL;
                break;
            default:
                $severity = OutputInterface::VERBOSITY_QUIET;
                break;
        }

        $output->writeln($message, $severity | $outputMode);
    }
}
